# == Schema Information
#
# Table name: tweets
#
#  id         :integer          not null, primary key
#  body       :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#
# Foreign Keys
#
#  user_id  (user_id => users.id)
#
class Tweet < ApplicationRecord
  belongs_to :user

  validates :body, length: { maximum: 180 }
  before_validation :check_24h_duplicity

  scope :without_company, -> { joins(:user).where('users.company_id is null') }

  private

  def check_24h_duplicity
    return unless Tweet.where(body:, user_id:, created_at: Time.current - 24.hours..Time.current).count.positive?

    raise StandardError('A tweet was created with the same body in the last 24 hours')
  end
end
